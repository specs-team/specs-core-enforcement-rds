package eu.specs.project.enforcement.rds_api.controller;

import eu.specs.datamodel.common.ResourceCollection;
import eu.specs.datamodel.enforcement.RemActivity;
import eu.specs.project.enforcement.rds.core.service.RemActivityService;
import eu.specs.project.enforcement.rds_api.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.util.List;

@Controller
@RequestMapping(value = "/rem-activities")
public class RemActivityController {
    private static final String REM_ACT_PATH = "/rem-activities/{id}";

    @Autowired
    private RemActivityService remActivityService;

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(value = HttpStatus.CREATED)
    public ResponseEntity<RemActivity> createRemActivity(@RequestBody RemActivity data,
                                                         HttpServletRequest request) {

        RemActivity remActivity = remActivityService.create(data);

        URI location = URI.create(request.getRequestURL().append("/").append(remActivity.getId()).toString());
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(location);
        return new ResponseEntity<>(remActivity, headers, HttpStatus.CREATED);
    }

    @RequestMapping(method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public ResourceCollection getAllRemActivities(
            @RequestParam(value = "slaId", required = false) String slaId,
            @RequestParam(value = "offset", required = false) Integer offset,
            @RequestParam(value = "limit", required = false) Integer limit) {

        RemActivityService.RemActivityFilter filter = new RemActivityService.RemActivityFilter();
        filter.setSlaId(slaId);
        filter.setOffset(offset);
        filter.setLimit(limit);
        List<RemActivity> remActivities = remActivityService.findAll(filter);
        ResourceCollection collection = new ResourceCollection();
        for (RemActivity remActivity : remActivities) {
            URI uri = ServletUriComponentsBuilder.fromCurrentContextPath()
                    .pathSegment(REM_ACT_PATH)
                    .buildAndExpand(remActivity.getId())
                    .toUri();
            collection.addItem(new ResourceCollection.Item(remActivity.getId(), uri.toString()));
        }
        collection.setResource("rem-activities");
        collection.setTotal(remActivities.size());
        return collection;
    }

    @RequestMapping(value = "/{remActId}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public RemActivity getRemActivity(@PathVariable("remActId") String remActId) throws ResourceNotFoundException {
        RemActivity remActivity = remActivityService.get(remActId);
        if (remActivity == null) {
            throw new ResourceNotFoundException(String.format("Remediation activity %s cannot be found.", remActId));
        } else {
            return remActivity;
        }
    }

    @RequestMapping(value = "/{remActId}/status", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public String getRemActivityStatus(@PathVariable("remActId") String remActId) throws ResourceNotFoundException {
        RemActivity remActivity = remActivityService.get(remActId);
        if (remActivity == null) {
            throw new ResourceNotFoundException(String.format("Remediation activity %s cannot be found.", remActId));
        } else {
            return remActivity.getState().name();
        }
    }
}
