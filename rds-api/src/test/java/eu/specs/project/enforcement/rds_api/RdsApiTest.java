package eu.specs.project.enforcement.rds_api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import eu.specs.datamodel.enforcement.RemActivity;
import eu.specs.project.enforcement.rds_api.mock.ImplementationMock;
import eu.specs.project.enforcement.rds_api.mock.ServiceManagerMock;
import eu.specs.project.enforcement.rds_api.mock.SlaManagerMock;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.util.SocketUtils;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.util.Date;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration("classpath:rds-api-context-test.xml")
public class RdsApiTest extends TestParent {
    private static final Logger logger = LogManager.getLogger(RdsApiTest.class);

    @Autowired
    private ImplementationMock implementationMock;

    @Autowired
    private ServiceManagerMock serviceManagerMock;

    @Autowired
    private SlaManagerMock slaManagerMock;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(Integer.parseInt(System.getProperty("wiremock.port")));

    @BeforeClass
    public static void setSystemProps() {
        int port = SocketUtils.findAvailableTcpPort();
        logger.debug("Using port {} for WireMock server.", port);
        System.setProperty("wiremock.port", Integer.toString(port));
    }

    @Before
    public void setUp() throws IOException {
        this.mockMvc = webAppContextSetup(this.wac).build();
        implementationMock.init();
        serviceManagerMock.init();
        slaManagerMock.init();
    }

    @Test
    public void testRemediationApi() throws Exception {
        RemActivity remActivityData = objectMapper.readValue(
                this.getClass().getResourceAsStream("/rem-activity-data.json"), RemActivity.class);

        MvcResult result = mockMvc.perform(post("/rem-activities")
                .content(objectMapper.writeValueAsString(remActivityData)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn();

        RemActivity remActivity = objectMapper.readValue(
                result.getResponse().getContentAsString(), RemActivity.class);

        assertNotNull(remActivity.getId());
        assertEquals(remActivity.getSlaId(), remActivityData.getSlaId());

        // wait until the remediation activity finishes
        Date startTime = new Date();
        RemActivity.Status status;
        do {
            logger.trace("Waiting for the remediation activity to finish...");
            Thread.sleep(1000);
            result = mockMvc.perform(get("/rem-activities/{0}/status", remActivity.getId()))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andReturn();
            status = RemActivity.Status.valueOf(
                    result.getResponse().getContentAsString());

            if (status == RemActivity.Status.ERROR) {
                fail("Remediation activity failed.");
            }
            if (new Date().getTime() - startTime.getTime() > 15000) {
                fail("Timeout waiting for the remediation activity to finish.");
            }
        } while (status != RemActivity.Status.SOLVED);

        mockMvc.perform(get("/rem-activities")
                .param("slaId", remActivity.getSlaId())
                .param("offset", "0")
                .param("limit", "1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.resource", is("rem-activities")))
                .andExpect(jsonPath("$.total", is(1)));

        mockMvc.perform(get("/rem-activities/{0}", remActivity.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.rem_act_id", is(remActivity.getId())));
    }
}
