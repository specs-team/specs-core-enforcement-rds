package eu.specs.project.enforcement.rds_api;

import com.github.fakemongo.Fongo;
import com.mongodb.DB;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration("classpath:rds-api-context-test.xml")
public abstract class TestParent {
    private static final Logger logger = LogManager.getLogger(TestParent.class);

    @Autowired
    public Fongo fongo;

    @After
    public void tearDown() throws Exception {
        for (DB db : fongo.getUsedDatabases()) {
            logger.trace("Dropping database " + db.getName());
            db.dropDatabase();
        }
    }
}
