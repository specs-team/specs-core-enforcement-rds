package eu.specs.project.enforcement.rds_api.mock;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.io.Resources;
import eu.specs.datamodel.enforcement.SecurityMechanism;
import eu.specs.project.enforcement.rds.core.client.ServiceManagerClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

@Component
public class ServiceManagerMock {

    @Autowired
    private ObjectMapper objectMapper;

    public void init() throws IOException {
        ServiceManagerClient.CollectionSchema collection = new ServiceManagerClient.CollectionSchema();
        collection.setTotal(1);
        String sm2Url = String.format("http://localhost:%s/service-manager/cloud-sla/security-mechanisms/SM2",
                System.getProperty("wiremock.port"));
        collection.setItemList(Arrays.asList(sm2Url));

        stubFor(get(urlPathEqualTo("/service-manager/cloud-sla/security-mechanisms"))
                .withQueryParam("metrics_id", containing("basic_scan_frequency_m13"))
                .withQueryParam("metrics_id", containing("extended_scan_frequency_m22"))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(objectMapper.writeValueAsString(collection))));

        String mechanismSvaJson = Resources.toString(
                this.getClass().getResource("/mechanism-sva.json"), Charset.forName("UTF-8"));
        SecurityMechanism mechanismSva = objectMapper.readValue(mechanismSvaJson, SecurityMechanism.class);

        stubFor(get(urlPathEqualTo("/service-manager/cloud-sla/security-mechanisms/" + mechanismSva.getId()))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(mechanismSvaJson)));
    }
}
