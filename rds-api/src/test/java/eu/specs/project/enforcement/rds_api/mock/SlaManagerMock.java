package eu.specs.project.enforcement.rds_api.mock;

import org.springframework.stereotype.Component;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

@Component
public class SlaManagerMock {

    public void init() {

        stubFor(put(urlPathMatching("/sla-manager/cloud-sla/slas/[\\w-]+/status"))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(204)));

        stubFor(post(urlPathMatching("/sla-manager/cloud-sla/slas/[\\w-]+/(redress|remediate|observe)"))
                .willReturn(aResponse()
                        .withStatus(200)));

    }
}
