package eu.specs.project.enforcement.rds.core.client;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import eu.specs.datamodel.enforcement.ImplementationPlan;
import eu.specs.datamodel.enforcement.RemPlan;
import eu.specs.project.enforcement.rds.core.exception.RemediationException;
import eu.specs.project.enforcement.rds.core.util.AppConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import java.util.Map;

@Component
public class ImplementationClient {
    private static String IMPL_PLAN_PATH = "/impl-plans/{id}";
    private static String REM_PLANS_PATH = "/rem-plans";
    private static String REM_PLANS_RESULT_PATH = "/rem-plans/{id}/result";
    private static final Logger logger = LogManager.getLogger(ImplementationClient.class);

    private WebTarget implementationTarget;

    @Autowired
    public ImplementationClient(AppConfig appConfig) {
        Client client = ClientBuilder.newBuilder()
                .register(JacksonJsonProvider.class)
                .build();

        implementationTarget = client.target(appConfig.getImplementationApiAddress());
    }

    public ImplementationPlan retrieveImplPlan(String implPlanId) throws RemediationException {
        try {
            logger.debug("Retrieving implementation plan {}...", implPlanId);
            ImplementationPlan implPlan = implementationTarget
                    .path(IMPL_PLAN_PATH)
                    .resolveTemplate("id", implPlanId)
                    .request(MediaType.APPLICATION_JSON_TYPE)
                    .get(ImplementationPlan.class);

            logger.debug("Implementation plan retrieved successfully.");
            return implPlan;
        } catch (Exception e) {
            throw new RemediationException("Failed to retrieve implementation plan: " + e.getMessage(), e);
        }
    }

    public String implementRemPlan(RemPlan remPlan) throws RemediationException {
        try {
            logger.debug("Calling Implementating to implement remediation plan...");
            Map<String, String> remPlanResponse = implementationTarget
                    .path(REM_PLANS_PATH)
                    .request(MediaType.APPLICATION_JSON_TYPE)
                    .post(Entity.json(remPlan), new GenericType<Map<String, String>>() {
                    });

            String remPlanId = remPlanResponse.get("rem_plan_id");
            logger.debug("Remediation plan ID returned by the Implementation: {}", remPlanId);
            logger.debug("implementRemPlan() finished successfully.");
            return remPlanId;
        } catch (Exception e) {
            throw new RemediationException(
                    String.format("Failed to implement remediation plan %s: %s", remPlan.getId(), e.getMessage()), e);
        }
    }

    public RemPlan.Result retrieveRemPlanResult(String remPlanId) throws RemediationException {
        try {
            Map<String, RemPlan.Result> resultResponse = implementationTarget
                    .path(REM_PLANS_RESULT_PATH)
                    .resolveTemplate("id", remPlanId)
                    .request(MediaType.APPLICATION_JSON)
                    .get(new GenericType<Map<String, RemPlan.Result>>() {
                    });

            RemPlan.Result result = resultResponse.get("result");
            return result;

        } catch (Exception e) {
            throw new RemediationException("Failed to retrieve remediation plan result: " + e.getMessage(), e);
        }
    }
}
