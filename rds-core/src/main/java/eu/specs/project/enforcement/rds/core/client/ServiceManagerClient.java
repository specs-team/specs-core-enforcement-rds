package eu.specs.project.enforcement.rds.core.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import eu.specs.datamodel.enforcement.SecurityMechanism;
import eu.specs.project.enforcement.rds.core.exception.RemediationException;
import eu.specs.project.enforcement.rds.core.util.AppConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

@Component
public class ServiceManagerClient {
    private static final Logger logger = LogManager.getLogger(ServiceManagerClient.class);
    private static String MECHANISMS_PATH = "/security-mechanisms";

    private Client client;
    private WebTarget serviceManagerTarget;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    public ServiceManagerClient(AppConfig appConfig) {
        client = ClientBuilder.newBuilder()
                .register(JacksonJsonProvider.class)
                .build();

        serviceManagerTarget = client.target(appConfig.getServiceManagerApiAddress());
    }

    public List<SecurityMechanism> getSecurityMechanisms(List<String> metrics) throws RemediationException {
        logger.debug("Retrieving security mechanism for metrics {}...", metrics);

        try {
            String metricsJson = objectMapper.writeValueAsString(metrics);

            CollectionSchema collection = serviceManagerTarget
                    .path(MECHANISMS_PATH)
                    .queryParam("metrics_id", metricsJson)
                    .request(MediaType.APPLICATION_JSON)
                    .get(CollectionSchema.class);

            logger.debug("Security mechanisms: {}", collection.getItemList());

            List<SecurityMechanism> mechanisms = new ArrayList<>();

            if (collection.getTotal() > 0) {
                for (String smUrl : collection.getItemList()) {
                    SecurityMechanism mechanism = client
                            .target(smUrl)
                            .request(MediaType.APPLICATION_JSON)
                            .get(SecurityMechanism.class);
                    mechanisms.add(mechanism);
                }
            }

            logger.debug("Security mechanisms have been retrieved successfully.");
            return mechanisms;

        } catch (Exception e) {
            throw new RemediationException(String.format(
                    "Failed to retrieve security mechanisms for metrics %s: %s", metrics, e.getMessage()), e);
        }
    }

    public static class CollectionSchema {
        private String resource;
        private Integer total;
        private Integer members;
        private List<String> itemList;

        public String getResource() {
            return resource;
        }

        public void setResource(String resource) {
            this.resource = resource;
        }

        public Integer getTotal() {
            return total;
        }

        public void setTotal(Integer total) {
            this.total = total;
        }

        public Integer getMembers() {
            return members;
        }

        public void setMembers(Integer members) {
            this.members = members;
        }

        public List<String> getItemList() {
            return itemList;
        }

        public void setItemList(List<String> itemList) {
            this.itemList = itemList;
        }
    }
}