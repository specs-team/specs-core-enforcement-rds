package eu.specs.project.enforcement.rds.core.client;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import eu.specs.datamodel.common.SlaState;
import eu.specs.project.enforcement.rds.core.exception.RemediationException;
import eu.specs.project.enforcement.rds.core.util.AppConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.Map;

@Component
public class SlaManagerClient {
    private static String SLA_PATH = "/slas/{id}";
    private static final Logger logger = LogManager.getLogger(SlaManagerClient.class);

    private WebTarget slaManagerTarget;
    private Map<SlaState, String> slaStateCommands;

    @Autowired
    public SlaManagerClient(AppConfig appConfig) {
        Client client = ClientBuilder.newBuilder()
                .register(JacksonJsonProvider.class)
                .build();

        slaManagerTarget = client.target(appConfig.getSlaManagerApiAddress());

        slaStateCommands = new HashMap<>();
        slaStateCommands.put(SlaState.PROACTIVE_REDRESSING, "redress");
        slaStateCommands.put(SlaState.REMEDIATING, "remediate");
        slaStateCommands.put(SlaState.OBSERVED, "observe");
    }

    public void setSlaState(String slaId, SlaState slaState) throws RemediationException {
        logger.debug("Setting SLA {} state to {}...", slaId, slaState);
        String command = slaStateCommands.get(slaState);
        if (command == null) {
            throw new UnsupportedOperationException("Invalid SLA state: " + slaState);
        }

        try {
            Response response = slaManagerTarget
                    .path(SLA_PATH + "/" + slaStateCommands.get(slaState))
                    .resolveTemplate("id", slaId)
                    .request()
                    .post(Entity.json(null));

            if (response.getStatusInfo().getFamily() != Response.Status.Family.SUCCESSFUL) {
                String content = response.readEntity(String.class);
                logger.error("Failed to set SLA state: {}\n{}", response.getStatusInfo(), content);
                throw new RemediationException(String.format("Failed to set SLA state to %s.", slaState));
            } else {
                logger.debug("SLA state set successfully.");
            }
        } catch (Exception e) {
            throw new RemediationException(String.format("Failed to set SLA state: %s",
                    e.getMessage()), e);
        }
    }
}
