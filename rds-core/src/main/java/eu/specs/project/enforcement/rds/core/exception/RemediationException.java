package eu.specs.project.enforcement.rds.core.exception;

public class RemediationException extends Exception {

    public RemediationException() {
        super();
    }

    public RemediationException(String message) {
        super(message);
    }

    public RemediationException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
