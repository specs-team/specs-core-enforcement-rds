package eu.specs.project.enforcement.rds.core.processor;

import eu.specs.datamodel.common.Annotation;
import eu.specs.datamodel.common.SlaState;
import eu.specs.datamodel.enforcement.*;
import eu.specs.project.enforcement.rds.core.client.ImplementationClient;
import eu.specs.project.enforcement.rds.core.client.ServiceManagerClient;
import eu.specs.project.enforcement.rds.core.client.SlaManagerClient;
import eu.specs.project.enforcement.rds.core.exception.RemediationException;
import eu.specs.project.enforcement.rds.core.repository.RemActivityRepository;
import eu.specs.project.enforcement.rds.core.util.AppConfig;
import eu.specs.project.enforcement.rds.core.util.JsonDumper;
import eu.specsproject.core.slaplatform.auditing.client.AuditClient;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class RemActivityProcessor {
    private static final Logger logger = LogManager.getLogger(RemActivityProcessor.class);

    @Autowired
    private RemActivityRepository remActivityRepository;

    @Autowired
    private SlaManagerClient slaManagerClient;

    @Autowired
    private ServiceManagerClient serviceManagerClient;

    @Autowired
    private ImplementationClient implementationClient;

    @Autowired
    private AppConfig appConfig;

    @Autowired
    private AuditClient auditClient;

    public void processRemActivity(RemActivity remActivity) {

        try {
            logger.debug("Processing remediation activity {}...", remActivity.getId());

            processRemActivityImpl(remActivity);

            logger.debug("Remediation activity has finished successfully.");
            if (logger.isTraceEnabled()) {
                logger.debug("Remediation activity:\n{}", JsonDumper.dump(remActivity));
            }

        } catch (Exception e) {
            logger.error("Remediation activity failed: {}", e.getMessage(), e);
            remActivity.setState(RemActivity.Status.ERROR);
            remActivity.addAnnotation(
                    new Annotation("error", "Remediation activity failed: " + e.getMessage(), new Date()));
            remActivityRepository.save(remActivity);
        }
    }

    private void processRemActivityImpl(RemActivity remActivity) throws RemediationException, InterruptedException {
        String slaId = remActivity.getSlaId();
        SlaState slaState;
        if (remActivity.getClassification() == RemActivity.Classification.ALERT) {
            slaState = SlaState.PROACTIVE_REDRESSING;
        } else if (remActivity.getClassification() == RemActivity.Classification.VIOLATION) {
            slaState = SlaState.REMEDIATING;
        } else {
            throw new RemediationException("Unexpected remediation activity classification: " + remActivity.getClassification());
        }

        auditClient.logComponentActivity("RDS", CompActivity.ComponentState.ACTIVATED, slaId, slaState);

        remActivity.setState(RemActivity.Status.REMEDIATING);
        remActivityRepository.save(remActivity);

        slaManagerClient.setSlaState(slaId, slaState);

        ImplementationPlan implPlan = implementationClient.retrieveImplPlan(remActivity.getPlanId());

        // prepare SLOs map
        Map<String, Slo> slosMap = new HashMap<>();
        for (Slo slo : implPlan.getSlos()) {
            slosMap.put(slo.getId(), slo);
        }

        List<String> affectedMetrics = new ArrayList<>();
        for (String affectedSloId : remActivity.getAffectedSlos()) {
            Slo affectedSlo = slosMap.get(affectedSloId);
            if (affectedSlo == null) {
                throw new RemediationException(String.format(
                        "SLO with ID %s cannot be found in the implementation plan.", affectedSloId));
            }
            affectedMetrics.add(affectedSlo.getMetricId());
        }
        logger.debug("Affected metrics: {}", affectedMetrics);

        List<SecurityMechanism> mechanisms = serviceManagerClient.getSecurityMechanisms(affectedMetrics);
        if (mechanisms.isEmpty()) {
            throw new RemediationException(String.format("No security mechanism found for the metrics %s.", affectedMetrics));
        } else if (mechanisms.size() > 1) {
            throw new RemediationException(String.format(
                    "Multiple security mechanisms found for the metrics %s. Exactly one expected.", affectedMetrics));
        }
        SecurityMechanism mechanism = mechanisms.get(0);
        logger.debug("Using security mechanism {}.", mechanism.getId());

        // find remediation flow
        RemFlow remFlow = null;
        for (RemFlow remFlow1 : mechanism.getRemediation().getRemFlows()) {
            if (remFlow1.getName().equals(remActivity.getEventId())) {
                remFlow = remFlow1;
                break;
            }
        }
        if (remFlow == null) {
            throw new RemediationException(String.format(
                    "No remediation flow found for the event %s in the security mechanism %s.",
                    remActivity.getEventId(), mechanism.getId()));
        }

        logger.debug("Found remediation flow {} for the event {}.", remFlow.getName(), remActivity.getEventId());

        // prepare remediation actions map
        Map<String, RemAction> remActionMap = new HashMap<>();
        for (RemAction remAction : mechanism.getRemediation().getRemActions()) {
            remActionMap.put(remAction.getName(), remAction);
        }

        // prepare RemAction collection
        Set<RemAction> remActions = new HashSet<>();
        addRemActions(remFlow, remActions, remActionMap);

        // prepare Chef recipes map
        Map<String, ChefRecipe> chefRecipeMap = new HashMap<>();
        for (ChefRecipe chefRecipe : mechanism.getChefRecipes()) {
            chefRecipeMap.put(chefRecipe.getName(), chefRecipe);
        }

        // prepare ChefRecipe colleciton
        Set<ChefRecipe> chefRecipes = new HashSet<>();
        for (RemAction remAction : remActions) {
            for (String chefRecipeId : remAction.getRecipes()) {
                ChefRecipe chefRecipe = chefRecipeMap.get(chefRecipeId);
                if (chefRecipe == null) {
                    throw new RemediationException(String.format(
                            "Chef recipe %s cannot be found in the security mechanism.", chefRecipeId));
                }
                chefRecipes.add(chefRecipe);
            }
        }

        RemPlan remPlan = createRemPlan(remActivity, remFlow, remActions, chefRecipes);
        logger.debug("Remediation plan created successfully.");
        if (logger.isTraceEnabled()) {
            logger.trace("Remediation plan:\n{}", JsonDumper.dump(remPlan));
        }

        // implement remediation plan
        String remPlanId = implementationClient.implementRemPlan(remPlan);

        remActivity.setRemPlanId(remPlanId);
        remActivityRepository.save(remActivity);

        // wait until remediation plan finishes
        logger.debug("Waiting for the remediation plan to finish...");
        Date startTime = new Date();
        RemPlan.Result remPlanResult = null;
        while (true) {
            remPlanResult = implementationClient.retrieveRemPlanResult(remPlanId);
            logger.debug("Remediation plan result: {}", (remPlanResult == null) ? "N/A" : remPlanResult);

            if (remPlanResult != null) {
                break;
            }
            Thread.sleep(10000);
            if (new Date().getTime() - startTime.getTime() > appConfig.getRemPlanTimeout() * 1000) {
                throw new RemediationException("Timeout waiting for the remediation plan to finish.");
            }
        }

        logger.debug("Remediation plan finished with the result {}", remPlanResult);
        if (remPlanResult == RemPlan.Result.OBSERVE) {
            slaManagerClient.setSlaState(slaId, SlaState.OBSERVED);
        } else if (remPlanResult == RemPlan.Result.NOTIFY) {
            // TODO: notify SPECS App
        } else if (remPlanResult == RemPlan.Result.ERROR) {
            throw new RemediationException(String.format("Failed to implement remediation plan %s.", remPlanId));
        } else {
            throw new UnsupportedOperationException("Invalid remPlanResult: " + remPlanResult);
        }

        auditClient.logComponentActivity("RDS", CompActivity.ComponentState.DEACTIVATED, slaId, SlaState.OBSERVED);

        remActivity.setState(RemActivity.Status.SOLVED);
        remActivity.setResult(remPlanResult);
        remActivityRepository.save(remActivity);
    }

    private void addRemActions(Object remFlowObj, Set<RemAction> remActions, Map<String, RemAction> remActionMap) throws RemediationException {
        if (remFlowObj instanceof RemFlow) {
            RemFlow remFlow = (RemFlow) remFlowObj;
            RemAction remAction = remActionMap.get(remFlow.getActionId());
            if (remAction == null) {
                throw new RemediationException(String.format(
                        "The remediation action %s cannot be found in the security mechanism.", remFlow.getActionId()));
            }
            remActions.add(remAction);
            addRemActions(remFlow.getYesAction(), remActions, remActionMap);
            addRemActions(remFlow.getNoAction(), remActions, remActionMap);
        } else if (remFlowObj instanceof Map) {
            Map remFlow = (Map) remFlowObj;
            String actionId = (String) remFlow.get("action_id");
            Object yesAction = remFlow.get("yes_action");
            Object noAction = remFlow.get("no_action");

            RemAction remAction = remActionMap.get(actionId);
            if (remAction == null) {
                throw new RemediationException(String.format(
                        "The remediation action %s cannot be found in the security mechanism.", actionId));
            }
            remActions.add(remAction);
            addRemActions(yesAction, remActions, remActionMap);
            addRemActions(noAction, remActions, remActionMap);
        } else if (remFlowObj instanceof String) {
            return;
        } else {
            throw new RemediationException("Unexpected RemFlow object type: " + remFlowObj.getClass().getName());
        }
    }

    private RemPlan createRemPlan(RemActivity remActivity, RemFlow remFlow,
                                  Collection<RemAction> remActions, Collection<ChefRecipe> chefRecipes) {
        RemPlan remPlan = new RemPlan();
        remPlan.setCreationTime(new Date());
        remPlan.setSlaId(remActivity.getSlaId());
        remPlan.setPlanId(remActivity.getPlanId());
        remPlan.setEventId(remActivity.getEventId());
        remPlan.setRootCauses(remActivity.getRootCauses());
        remPlan.setRemFlow(remFlow);
        remPlan.getRemActions().addAll(remActions);
        remPlan.getChefRecipes().addAll(chefRecipes);
        return remPlan;
    }
}
