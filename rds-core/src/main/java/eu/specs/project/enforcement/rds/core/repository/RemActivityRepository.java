package eu.specs.project.enforcement.rds.core.repository;

import eu.specs.datamodel.enforcement.RemActivity;
import eu.specs.project.enforcement.rds.core.service.RemActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class RemActivityRepository {

    @Autowired
    MongoTemplate mongoTemplate;

    public RemActivity findById(String remActId) {
        return mongoTemplate.findById(remActId, RemActivity.class);
    }

    public List<RemActivity> findAll(RemActivityService.RemActivityFilter filter) {
        Query query = new Query();
        if (filter.getSlaId() != null) {
            query.addCriteria(Criteria.where("slaId").is(filter.getSlaId()));
        }
        if (filter.getLimit() != null) {
            query.limit(filter.getLimit());
        }
        if (filter.getOffset() != null) {
            query.skip(filter.getOffset());
        }

        query.fields().include("id");
        return mongoTemplate.find(query, RemActivity.class);
    }

    public void save(RemActivity remActivity) {
        mongoTemplate.save(remActivity);
    }

    public void update(RemActivity remActivity) {
        mongoTemplate.save(remActivity);
    }

    public void delete(RemActivity remActivity) {
        mongoTemplate.remove(remActivity);
    }
}
