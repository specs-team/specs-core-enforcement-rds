package eu.specs.project.enforcement.rds.core.service;

import eu.specs.datamodel.enforcement.RemActivity;
import eu.specs.project.enforcement.rds.core.processor.RemActivityProcessor;
import eu.specs.project.enforcement.rds.core.repository.RemActivityRepository;
import eu.specs.project.enforcement.rds.core.util.JsonDumper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
public class RemActivityService {
    private static final Logger logger = LogManager.getLogger(RemActivityService.class);
    public static final int EXECUTOR_NUMBER_OF_THREADS = 3;
    private ExecutorService executorService;

    @Autowired
    private RemActivityRepository remActivityRepository;

    @Autowired
    private RemActivityProcessor remActivityProcessor;

    public RemActivityService() {
        executorService = Executors.newFixedThreadPool(EXECUTOR_NUMBER_OF_THREADS);
    }

    public RemActivity create(final RemActivity remActivity) {
        logger.debug("Creating remediation activity...");
        remActivity.setId(UUID.randomUUID().toString());
        remActivity.setCreationTime(new Date());
        remActivity.setState(RemActivity.Status.CREATED);
        remActivityRepository.save(remActivity);

        if (logger.isTraceEnabled()) {
            logger.debug("Remediation activity has been created successfully:\n{}", JsonDumper.dump(remActivity));
        }

        executorService.execute(new Runnable() {
            @Override
            public void run() {
                remActivityProcessor.processRemActivity(remActivity);
            }
        });

        return remActivity;
    }

    public RemActivity get(String remActId) {
        return remActivityRepository.findById(remActId);
    }

    public List<RemActivity> findAll(RemActivityFilter filter) {
        return remActivityRepository.findAll(filter);
    }

    public static class RemActivityFilter {
        private String slaId;
        private Integer offset;
        private Integer limit;

        public String getSlaId() {
            return slaId;
        }

        public void setSlaId(String slaId) {
            this.slaId = slaId;
        }

        public Integer getOffset() {
            return offset;
        }

        public void setOffset(Integer offset) {
            this.offset = offset;
        }

        public Integer getLimit() {
            return limit;
        }

        public void setLimit(Integer limit) {
            this.limit = limit;
        }
    }
}
