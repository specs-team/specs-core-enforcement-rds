package eu.specs.project.enforcement.rds.core.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class AppConfig {

    @Value("${sla-manager-api.address}")
    private String slaManagerApiAddress;

    @Value("${service-manager-api.address}")
    private String serviceManagerApiAddress;

    @Value("${implementation-api.address}")
    private String implementationApiAddress;

    @Value("${auditing-api.address}")
    private String auditingApiAddress;

    @Value("${remediation-plan.timeout}")
    private int remPlanTimeout;

    public String getSlaManagerApiAddress() {
        return slaManagerApiAddress;
    }

    public String getServiceManagerApiAddress() {
        return serviceManagerApiAddress;
    }

    public String getImplementationApiAddress() {
        return implementationApiAddress;
    }

    public String getAuditingApiAddress() {
        return auditingApiAddress;
    }

    public int getRemPlanTimeout() {
        return remPlanTimeout;
    }
}
