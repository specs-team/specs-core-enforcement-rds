package eu.specs.project.enforcement.rds.core.mock;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.specs.datamodel.enforcement.ImplementationPlan;
import eu.specs.datamodel.enforcement.RemPlan;
import eu.specs.project.enforcement.rds.core.util.JsonDumper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

@Component
public class ImplementationMock {

    @Autowired
    private ObjectMapper objectMapper;

    public void init() throws IOException {
        String remPlanId = "92118138-1ebe-4098-98de-310eb305d39d";

        ImplementationPlan implPlan = objectMapper.readValue(
                this.getClass().getResourceAsStream("/impl-plan.json"), ImplementationPlan.class);

        stubFor(get(urlPathEqualTo("/implementation-api/impl-plans/" + implPlan.getId()))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(objectMapper.writeValueAsString(implPlan))));

        Map<String, String> postResponse = new HashMap<>();
        postResponse.put("rem_plan_id", remPlanId);
        stubFor(post(urlPathEqualTo("/implementation-api/rem-plans"))
                .withHeader("Content-Type", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(201)
                        .withHeader("Content-Type", "application/json")
                        .withBody(JsonDumper.dump(postResponse))));

        Map<String, RemPlan.Result> resultResponse = new HashMap<>();
        resultResponse.put("result", RemPlan.Result.OBSERVE);
        stubFor(get(urlPathEqualTo(String.format("/implementation-api/rem-plans/%s/result", remPlanId)))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(JsonDumper.dump(resultResponse))));
    }
}
