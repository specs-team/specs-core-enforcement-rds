package eu.specs.project.enforcement.rds.core.repository;

import eu.specs.datamodel.enforcement.RemActivity;
import eu.specs.project.enforcement.rds.core.TestParent;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:rds-core-context-test.xml")
public class RemActivityRepositoryTest extends TestParent {

    @BeforeClass
    public static void setSystemProps() {
        System.setProperty("wiremock.port", "0");
    }

    @Autowired
    private RemActivityRepository raRepository;

    @Test
    public void test() throws Exception {
        RemActivity remActivity = new RemActivity();
        remActivity.setId(UUID.randomUUID().toString());
        remActivity.setCreationTime(new Date());
        remActivity.setState(RemActivity.Status.CREATED);
        raRepository.save(remActivity);

        RemActivity remActivity1 = raRepository.findById(remActivity.getId());
        assertEquals(remActivity1.getId(), remActivity.getId());

        remActivity.setState(RemActivity.Status.REMEDIATING);
        raRepository.update(remActivity);
        remActivity1 = raRepository.findById(remActivity.getId());
        assertEquals(remActivity1.getState(), RemActivity.Status.REMEDIATING);

        raRepository.delete(remActivity);
        assertNull(raRepository.findById(remActivity.getId()));
    }
}