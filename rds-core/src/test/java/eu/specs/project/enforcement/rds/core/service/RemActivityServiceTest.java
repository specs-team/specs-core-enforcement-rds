package eu.specs.project.enforcement.rds.core.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import eu.specs.datamodel.enforcement.RemActivity;
import eu.specs.project.enforcement.rds.core.mock.ImplementationMock;
import eu.specs.project.enforcement.rds.core.mock.ServiceManagerMock;
import eu.specs.project.enforcement.rds.core.mock.SlaManagerMock;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.SocketUtils;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:rds-core-context-test.xml")
public class RemActivityServiceTest {
    private static final Logger logger = LogManager.getLogger(RemActivityServiceTest.class);

    @Autowired
    private RemActivityService remActivityService;

    @Autowired
    private ImplementationMock implementationMock;

    @Autowired
    private ServiceManagerMock serviceManagerMock;

    @Autowired
    private SlaManagerMock slaManagerMock;

    @Autowired
    private ObjectMapper objectMapper;

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(Integer.parseInt(System.getProperty("wiremock.port")));

    @BeforeClass
    public static void setSystemProps() {
        int port = SocketUtils.findAvailableTcpPort();
        logger.debug("Using port {} for WireMock server.", port);
        System.setProperty("wiremock.port", Integer.toString(port));
    }

    @Before
    public void setUp() throws Exception {
        implementationMock.init();
        serviceManagerMock.init();
        slaManagerMock.init();
    }

    @Test
    public void testRemediationCore() throws Exception {

        RemActivity remActivityData = objectMapper.readValue(
                this.getClass().getResourceAsStream("/rem-activity-data.json"), RemActivity.class);

        RemActivity remActivity = remActivityService.create(remActivityData);
        assertEquals(remActivity.getState(), RemActivity.Status.CREATED);

        Date startTime = new Date();
        while (remActivity.getState() != RemActivity.Status.SOLVED &&
                remActivity.getState() != RemActivity.Status.ERROR) {
            Thread.sleep(1000);
            if (new Date().getTime() - startTime.getTime() > 15000) {
                fail("Timeout waiting for the remediation activity.");
            }
        }

        assertEquals(remActivity.getState(), RemActivity.Status.SOLVED);

        RemActivityService.RemActivityFilter filter = new RemActivityService.RemActivityFilter();
        filter.setSlaId(remActivity.getSlaId());
        List<RemActivity> remActivities = remActivityService.findAll(filter);
        assertEquals(remActivities.size(), 1);
    }
}