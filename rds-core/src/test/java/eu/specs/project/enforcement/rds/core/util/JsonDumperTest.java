package eu.specs.project.enforcement.rds.core.util;

import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class JsonDumperTest {

    @BeforeClass
    public static void setSystemProps() {
        System.setProperty("wiremock.port", "0");
    }

    @Test
    public void testDump() throws Exception {
        List<String> arrayList = Arrays.asList("one", "two", "three");
        String json = JsonDumper.dump(arrayList);
        assertEquals(json, "[ \"one\", \"two\", \"three\" ]");
    }
}