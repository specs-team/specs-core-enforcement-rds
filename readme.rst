.. contents:: Table of Contents

========
Overview
========

The SPECS Core Enforcement RDS (Remediation Decision System) component identifies proper proactive/corrective actions in the second step of the remediation phase to mitigate the risk of having a violation or to recover from one and prepares remediation plans according to results of the diagnosis process. The remediation plan is later executed by the Implementation component. The component functionalities are:

- determines root causes of monitoring events
- searches for redressing techniques
- builds remediation plan

The SLA remediation phase and the detailed remediation process is presented with sequence diagrams in the figures below.

**SLA remediation phase:**

.. image:: https://bitbucket.org/specs-team/specs-core-enforcement-rds/raw/master/docs/images/rem_phase.png

**Remediation process:**

.. image:: https://bitbucket.org/specs-team/specs-core-enforcement-rds/raw/master/docs/images/remediation.png

More details about the component can be found in the deliverable 4.2.2 available  at `http://www.specs-project.eu/publications/public-deliverables/d4-2-2 <http://www.specs-project.eu/publications/public-deliverables/d4-2-2>`_.

============
Installation
============

Prerequisites:

- Java web container
- MongoDB
- Java 7

The RDS component is packaged as a web application archive (WAR) file with the name rds-api.war. The archive file can be built from source code or downloaded from the SPECS maven repository::

 https://nexus.services.ieat.ro/nexus/content/repositories/specs-snapshots/eu/specs-project/core/enforcement/rds-api/

To build the RDS component from source code you need the Apache Maven 3 tool. First clone the project from the Bitbucket repository using a Git client::

 git clone git@bitbucket.org:specs-team/specs-core-enforcement-rds.git

then go into the specs-core-enforcement-rds directory and run::

 mvn package

The rds-api.war file is located in the rds-api/target directory.

The rds-api.war file has to be deployed to a Java web container. For example, to deploy the application to Apache Tomcat 7, just copy the war file to the Tomcat webapps directory::

 cp rds-api/target/rds-api.war /var/lib/tomcat7/webapps/

The application configuration is located in the file *rds.properties* in the Java properties format. The file contains the following configuration properties:

.. code-block:: jproperties

 planning-api.address=http://localhost:8080/planning-api
 implementation-api.address=http://localhost:8080/implementation-api
 auditing-api.address=http://localhost:8080/sla-auditing
 sla-manager-api.address=http://localhost:8080/sla-manager/cloud-sla
 service-manager-api.address=http://localhost:8080/service-manager/cloud-sla
 specs-application-api.address=http://localhost:8080/specs-app-webcontainer/api

Make the necessary changes and restart the web container for changes to take effect. The RDS API should now be available at::

 https://<host>:<port>/rds-api

======
Notice
======

This product includes software developed at "XLAB d.o.o, Slovenia", as part of the "SPECS - Secure Provisioning of Cloud Services based on SLA Management" research project (an EC FP7-ICT Grant, agreement 610795).

- http://www.specs-project.eu/
- http://www.xlab.si/

Developers:

- Jolanda Modic, jolanda.modic@xlab.si
- Damjan Murn, damjan.murn@xlab.si

Copyright:

.. code-block:: 

 Copyright 2013-2015, XLAB d.o.o, Slovenia
    http://www.xlab.si

 SPECS Core Enforcement RDS is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License, version 3,
 as published by the Free Software Foundation.

 SPECS Core Enforcement RDS is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.